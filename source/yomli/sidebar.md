---
Title: Sidebar
Status: shared
Description: Maître philosophe, modeste éditeur, geek à ses heures et parfois développeur web. Aime le thé noir et le riz cantonnais.
vcard: https://yom.li/contact.vcf
Links: Email->/api/email/redirect/?m=Evp(g+bHDb_L | Pouet->/api/mastodon/intent/?toot&user=yomli&instance=mastodon.xyz&visibility=direct | Pourboire->https://liberapay.com/yomli/donate
---
